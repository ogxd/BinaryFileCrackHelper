﻿using System.Windows;
using System.Windows.Media;

namespace Defacto.Backoffice.BinaryCracker {
    /// <summary>
    /// Logique d'interaction pour App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Close_Click(object sender, RoutedEventArgs e)
        {
            FileTab fileTab = GetVisualParent<FileTab>(sender);
            fileTab.remove();
        }

        public static T GetVisualParent<T>(object childObject) where T : Visual
        {
            DependencyObject child = childObject as DependencyObject;
            // iteratively traverse the visual tree
            while ((child != null) && !(child is T))
            {
                child = VisualTreeHelper.GetParent(child);
            }
            return child as T;
        }
    }
}
