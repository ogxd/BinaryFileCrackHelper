﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Xml.Serialization;

namespace Defacto.Backoffice.BinaryCracker {

    [Serializable()]
    public class BinaryEntry : INotifyPropertyChanged {

        public byte[] block8;

        public void initialize() {
            if (block8 != null)
                initialize(_pos, block8);
        }

        public void initialize(long pos, byte[] block8) {

            this.block8 = block8;
            this.pos = pos;

            byte block1 = block8[0];
            byte[] block2 = SubArray(block8, 0, 2);
            byte[] block4 = SubArray(block8, 0, 4);

            tbyte = block1;
            tchar = (tbyte < 41) ? ByteToHexBitFiddle(tbyte) : ((char)tbyte).ToString();
            tchar2 = BitConverter.ToChar(block2, 0).ToString();
            tshort = BitConverter.ToInt16(block2, 0);
            tint = BitConverter.ToInt32(block4, 0);
            //byte[] block4r = new byte[4];
            //int i = 0;
            //foreach (byte b in block4) {
            //    block4r[i] = (byte)~b;
            //    i++;
            //}
            tintf = swapEndianness((uint)tint);
            tlong = BitConverter.ToInt64(block8, 0);
            tfloat = BitConverter.ToSingle(block4, 0);

            //tint = Convert.ToInt32(block);
            //tlong = Convert.ToInt64(block);
            //tfloat = Convert.ToSingle(block);

            byte caca = 2;
            caca ^= caca;
        }

        static uint swapEndianness(uint x) {
            return ((x & 0x000000ff) << 24) +  // First byte
                   ((x & 0x0000ff00) << 8) +   // Second byte
                   ((x & 0x00ff0000) >> 8) +   // Third byte
                   ((x & 0xff000000) >> 24);   // Fourth byte
        }

        static string ByteToHexBitFiddle(byte _byte)
        {
            char[] c = new char[2];
            int b;

                b = _byte >> 4;
                c[0] = (char)(55 + b + (((b - 10) >> 31) & -7));
                b = _byte & 0xF;
                c[1] = (char)(55 + b + (((b - 10) >> 31) & -7));

            return new string(c);
        }

        static T[] SubArray<T>(T[] data, int index, int length) {
            T[] result = new T[length];
            Array.Copy(data, index, result, 0, length);
            return result;
        }

        private bool _shown = true;
        public bool shown {
            get { return _shown; }
            set {
                _shown = value;
                RaisePropertyChanged("shown");
            }
        }

        private bool _mark = false;
        public bool mark {
            get { return _mark; }
            set {
                _mark = value;
                RaisePropertyChanged("mark");
            }
        }

        private string _tcomment;
        public string tcomment
        {
            get { return _tcomment; }
            set
            {
                _tcomment = value;
                RaisePropertyChanged("tcomment");
            }
        }

        public string tbinary
        {
            get
            {
                return Convert.ToString(block8[0], 2).PadLeft(8, '0');
            }
        }

        [field: NonSerializedAttribute()]
        private string _tchar;
        public string tchar {
            get { return _tchar; }
            set {
                _tchar = value;
                RaisePropertyChanged("tchar");
            }
        }

        [field: NonSerializedAttribute()]
        private string _tchar2;
        public string tchar2
        {
            get { return _tchar2; }
            set
            {
                _tchar2 = value;
                RaisePropertyChanged("tchar2");
            }
        }

        [field: NonSerializedAttribute()]
        private byte _tbyte;
        public byte tbyte {
            get { return _tbyte; }
            set {
                _tbyte = value;
                RaisePropertyChanged("tbyte");
            }
        }

        [field: NonSerializedAttribute()]
        private short _tshort;
        public short tshort {
            get { return _tshort; }
            set {
                _tshort = value;
                RaisePropertyChanged("tshort");
            }
        }

        [field: NonSerializedAttribute()]
        private int _tint;
        public int tint {
            get { return _tint; }
            set {
                _tint = value;
                RaisePropertyChanged("tint");
            }
        }

        [field: NonSerializedAttribute()]
        private uint _tintf;
        public uint tintf {
            get { return _tintf; }
            set {
                _tintf = value;
                RaisePropertyChanged("tintf");
            }
        }

        [field: NonSerializedAttribute()]
        private long _tlong;
        public long tlong {
            get { return _tlong; }
            set {
                _tlong = value;
                RaisePropertyChanged("tlong");
            }
        }

        [field: NonSerializedAttribute()]
        private float _tfloat;
        public float tfloat {
            get { return _tfloat; }
            set {
                _tfloat = value;
                RaisePropertyChanged("tfloat");
                RaisePropertyChanged("tfloatColor");
            }
        }

        public SolidColorBrush tlineColor {
            get { return (_tprop == null)? Brushes.Transparent : Helper.bytesToColors[Helper.propToBytes[_tprop.Name]]; }
        }

        public Color tfloatColor {
            get { return ((_tfloat >= Helper.floatMin && _tfloat <= Helper.floatMax) || (_tfloat <= -Helper.floatMin && _tfloat >= -Helper.floatMax)) ? Colors.White : Colors.Gray; }
        }

        public Color tcharColor {
            get { return (_tbyte >= Helper.charMin && _tbyte <= Helper.charMax) ? Colors.White : Colors.Gray; }
        }

        private PropertyInfo _tprop;
        public PropertyInfo tprop {
            get { return _tprop; }
            set {
                _tprop = value;
                RaisePropertyChanged("tobject");
                RaisePropertyChanged("kept");
                RaisePropertyChanged("tlineColor");
            }
        }
        public object tobject {
            get { return (_tprop == null)? null : _tprop.GetValue(this, null); }
        }

        public bool kept {
            get { return (_tprop == null) ? false : true; }
        }

        [field: NonSerializedAttribute()]
        private long _pos;
        public long pos {
            get { return _pos; }
            set {
                _pos = value;
                RaisePropertyChanged("pos");
            }
        }

        [field: NonSerializedAttribute()]
        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string prop) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }


    public static class Helper {
        public static readonly float floatMin = 0.001f;
        public static readonly float floatMax = 100;

        public static readonly byte charMin = 60;
        public static readonly byte charMax = 172;

        public static readonly Dictionary<byte, SolidColorBrush> bytesToColors = new Dictionary<byte, SolidColorBrush>() {
            { 1, Brushes.DarkBlue },
            { 2, Brushes.DarkMagenta },
            { 4, Brushes.DarkRed },
            { 8, Brushes.DarkOliveGreen }
        };

        public static readonly Dictionary<string, byte> propToBytes = new Dictionary<string, byte>() {
            { "tchar", 1},
            { "tchar2", 2 },
            { "tbyte", 1 },
            { "tshort", 2 },
            { "tint", 4 },
            { "tlong", 8 },
            { "tfloat", 4 },
            { "tdouble", 8 }
        };
    }

    [Serializable()]
    public class FileTabSettings {

        public bool _showTbinary = true;
        public bool _showTchar1 = true;
        public bool _showTchar2 = true;
        public bool _showTint32 = true;
        public bool _showTint64 = true;
        public bool _showTfloat32 = true;
        public bool _showTfloat64 = true;

    }

    /// <summary>
    /// Logique d'interaction pour File.xaml
    /// </summary>
    ///
    public partial class FileTab : TabItem {

        public List<BinaryEntry> entries = new List<BinaryEntry>();
        public FileTabSettings settings = new FileTabSettings();

        public string file;

        public bool showTbinary {
            get {
                return settings._showTbinary;
            }
            set {
                settings._showTbinary = value;
                table.Columns[1].Visibility = (value) ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public bool showTchar1 {
            get {
                return settings._showTchar1;
            }
            set {
                settings._showTchar1 = value;
                table.Columns[2].Visibility = (value) ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public bool showTchar2 {
            get {
                return settings._showTchar2;
            }
            set {
                settings._showTchar2 = value;
                table.Columns[3].Visibility = (value) ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public bool showTint32 {
            get {
                return settings._showTint32;
            }
            set {
                settings._showTint32 = value;
                table.Columns[6].Visibility = (value) ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public bool showTint64 {
            get {
                return settings._showTint64;
            }
            set {
                settings._showTint64 = value;
                table.Columns[7].Visibility = (value) ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public bool showTfloat32 {
            get {
                return settings._showTfloat32;
            }
            set {
                settings._showTfloat32 = value;
                table.Columns[8].Visibility = (value) ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public bool showTfloat64 {
            get {
                return settings._showTfloat64;
            }
            set {
                settings._showTfloat64 = value;
                table.Columns[9].Visibility = (value) ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public FileTab() {

            //Loaded += FileTab_Loaded;

            InitializeComponent();

            tableContextMenu.DataContext = this;

            table.Loaded += event_FileTabLoaded;

            Mouse.AddPreviewMouseMoveHandler(this, new MouseEventHandler(event_PreviewMouseUp));
        }

        private void event_PreviewMouseUp(object sender, MouseEventArgs e)
        {
            if (IsDragAndDropping && Mouse.PrimaryDevice.LeftButton == MouseButtonState.Released)
            {
                IsDragAndDropping = false;
                System.Drawing.Point pos = System.Windows.Forms.Cursor.Position;

                Window window = Window.GetWindow(this);

                if (pos.X < window.Left || pos.X > (window.Left + window.ActualWidth) || pos.Y < window.Top || pos.Y > (window.Top + window.ActualHeight))
                {
                    Console.WriteLine("Drag out of any window");
                    MainWindow newwindow = new MainWindow();
                    newwindow.Show();
                    ((MainWindow)window).tabControl.Items.Remove(this);
                    if (((MainWindow)window).tabControl.Items.Count == 0)
                        window.Close();
                    newwindow.tabControl.Items.Add(this);
                    newwindow.tabControl.SelectedItem = this;
                }
            }
        }

        private void event_RowClicked(object sender, MouseButtonEventArgs e) {

            BinaryEntry entry = (BinaryEntry)((DataGridRow)sender).DataContext;
            string propName = table.CurrentCell.Column.SortMemberPath;
            PropertyInfo propInfo = entry.GetType().GetProperty(propName);

            if (propInfo == null) return;

            object result = propInfo.GetValue(entry, null);

            byte size; 

            switch (propName) {

                case "pos":
                    entry.mark = !entry.mark;
                    break;

                case "tobject":
                    if (entry.tprop == null)
                        return;

                    size = Helper.propToBytes[entry.tprop.Name];

                    for (long i = entry.pos + 1; i < entry.pos + size; i++) {
                        entries[(int)i].shown = true;
                    }
                    entry.tprop = null;
                    break;

                case "tcomment":
                    break;

                default:
                    size = Helper.propToBytes[propName];
                    for (long i = entry.pos + 1; i < entry.pos + size; i++) {
                        entries[(int)i].shown = false;
                    }
                    entry.tprop = propInfo;
                    break;
            }

            UpdateAnnotations();
            markUnsaved(true);

        }

        public void markUnsaved(bool condition) {
            if (condition) {
                if (((string)this.Header)[0] != '*')
                    this.Header = "*" + this.Header;
            } else {
                if (!string.IsNullOrEmpty(this.file))
                    this.Header = this.file;
            }
        }

        public void open(string file) {
            BinaryReader b = new BinaryReader(File.Open(file, FileMode.Open));
            long pos = 0;
            int length = (int)b.BaseStream.Length;

            while (pos + 8 < length) {
                byte[] block = b.ReadBytes(8);
                BinaryEntry binentry = new BinaryEntry();
                binentry.initialize(pos, block);
                entries.Add(binentry);
                pos++;
                b.BaseStream.Seek(pos, SeekOrigin.Begin);
            }

            this.Header = file;
        }

        public void serialize(string file) {

            FileStream fs = new FileStream(file, FileMode.Create);

            BinaryFormatter bfor = new BinaryFormatter();
            bfor.Serialize(fs, new object[] { entries, settings });

            fs.Close();

            this.Header = file;
            this.file = file;
        }

        public void deserialize(string file) {

            BinaryFormatter bfor = new BinaryFormatter();

            FileStream fs = new FileStream(file, FileMode.Open);
            object[] objects = (object[])bfor.Deserialize(fs);
            entries = (List<BinaryEntry>)objects[0];
            settings = (FileTabSettings)objects[1];
            int i = 0;
            foreach (BinaryEntry entry in entries)
            {
                entry.pos = i;
                entry.initialize();
                i++;
            }

            fs.Close();

            this.Header = file;
            this.file = file;

            //FileTab_Loaded(null, null);
        }

        Canvas annotationCanvas;
        private void event_FileTabLoaded(object sender, RoutedEventArgs e) {

            // Scrollbar stuff
            ScrollBar scrollBar = GetVisualChild<ScrollBar>(table);
            if (scrollBar == null) {
                return;
            }
            annotationCanvas = (Canvas)scrollBar.Template.FindName("AnnotationCanvas", scrollBar);
            UpdateAnnotations();
        }

        public bool IsDragAndDropping = false;
        private void event_TabItemDrag(object sender, MouseEventArgs e)
        {
            var tabItem = e.Source as TabItem;

            if (tabItem == null)
                return;

            if (Mouse.PrimaryDevice.LeftButton == MouseButtonState.Pressed)
            {
                if (!IsDragAndDropping)
                {
                    IsDragAndDropping = true;

                    try
                    {
                        DragDrop.DoDragDrop(tabItem, tabItem, DragDropEffects.Move);

                    }
                    catch
                    {

                    }
                }
            }
        }

        public void GoToLine(int line)
        {
            if (line > entries.Count) line = entries.Count;

            table.ScrollIntoView(entries[line]);
        }

        int currentSearch = 0;
        public void Search(string key) {
            int imax = entries.Count;
            int c = 0;
            while (currentSearch < imax) {
                if (entries[currentSearch].tchar[0] == key[c]) {
                    c ++;
                    currentSearch += 2;
                    if (c >= key.Length) {
                        GoToLine(currentSearch);
                        return;
                    }
                } else {
                    c = 0;
                    currentSearch++;
                }
            }
            currentSearch = 0;
        }

        private T GetVisualChild<T>(DependencyObject parent) where T : Visual {
            T child = default(T);

            int numVisuals = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < numVisuals; i++) {
                Visual v = (Visual)VisualTreeHelper.GetChild(parent, i);
                child = v as T;
                if (child == null) {
                    child = GetVisualChild<T>(v);
                }
                if (child != null) {
                    break;
                }
            }
            return child;
        }

        private void UpdateAnnotations() {
            if (annotationCanvas == null)
                return;

            annotationCanvas.Children.Clear();

            int i = 0;
            double m = entries.Count;
            double height = table.ActualHeight;

            foreach (BinaryEntry entry in entries) {
                if (entry.tobject != null) {
                    int p = (int)(height * i / m);
                    annotationCanvas.Children.Add(new Line() { X1 = 3, Y1 = p, X2 = 100, Y2 = p, StrokeThickness = 1, Stroke = entry.tlineColor });
                }
                if (!entry.shown) {
                    //m--;
                } else {
                    
                }
                if (entry.mark) {
                    int p = (int)(height * i / m);
                    annotationCanvas.Children.Add(new Line() { X1 = 0, Y1 = p, X2 = 100, Y2 = p, StrokeThickness = 1, Stroke = Brushes.White });
                }
                if (!string.IsNullOrEmpty(entry.tcomment))
                {
                    int p = (int)(height * i / m);
                    Marker marker = new Marker(entry.tcomment);
                    Canvas.SetTop(marker, p);
                    annotationCanvas.Children.Add( marker );
                }
                i++;
            }
        }

        public void remove()
        {
            ((TabControl)this.Parent).Items.Remove(this);
        }

        public void Close_Click(object sender, EventArgs e) {
            this.Visibility = Visibility.Collapsed;
        }
    }
}
