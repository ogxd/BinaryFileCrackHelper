﻿using MahApps.Metro.Controls;
using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Defacto.Backoffice.BinaryCracker {

    public static class Commands {
        public static RoutedCommand Save = new RoutedCommand();
    }

    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow {

        public MainWindow() {
            InitializeComponent();

            Commands.Save.InputGestures.Add(new KeyGesture(Key.S, ModifierKeys.Control));

            string input = @"G:\Daher\quad.cgr";

            string[] args = Environment.GetCommandLineArgs();
            if (args.Length > 1) input = args[1];

            this.Title = "Binary Cracker";
        }

        private void command_Save(object sender, ExecutedRoutedEventArgs e) {
            event_Save(null, null);
        }

        private void event_Save(object sender, RoutedEventArgs e) {

            FileTab newtab = (FileTab)tabControl.SelectedItem;

            if (newtab == null)
                return;

            if (string.IsNullOrEmpty(newtab.file)) {
                saveAs(newtab);
            } else {
                save(newtab, newtab.file);
            }
        }

        private void event_SaveAs(object sender, RoutedEventArgs e) {

            FileTab newtab = (FileTab)tabControl.SelectedItem;

            saveAs(newtab);
        }

        private void save(FileTab newtab, string path) {

            newtab.serialize(path);
            newtab.markUnsaved(false);
        }

        private void saveAs(FileTab newtab) {
            System.Windows.Forms.SaveFileDialog dialog = new System.Windows.Forms.SaveFileDialog();
            dialog.Filter = "Crack file|*.crack"; // To Add : PLY, WRL
            dialog.InitialDirectory = "L:\\";
            dialog.Title = "Save crack file";

            if (dialog.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                return;

            save(newtab, dialog.FileName);
        }

        private void event_Open(object sender, RoutedEventArgs e) {

            System.Windows.Forms.OpenFileDialog dialog = new System.Windows.Forms.OpenFileDialog();
            dialog.Multiselect = true;
            dialog.Filter = "Any file|*"; // To Add : PLY, WRL
            dialog.InitialDirectory = "L:\\";
            dialog.Title = "Select a file to crack";

            if (dialog.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                return;

            foreach (string file in dialog.FileNames) {
                if (Path.GetExtension(file).ToLower() != ".crack")
                {
                    FileTab newtab = new FileTab();
                    newtab.open(file);
                    tabControl.Items.Add(newtab);
                    tabControl.SelectedItem = newtab;
                    newtab.table.ItemsSource = newtab.entries;
                } else {
                    FileTab newtab = new FileTab();
                    newtab.deserialize(file);
                    tabControl.Items.Add(newtab);
                    tabControl.SelectedItem = newtab;
                    newtab.table.ItemsSource = newtab.entries;
                }
            }
        }

        private FileTab GetTargetTabItem(object originalSource)
        {
            var current = originalSource as DependencyObject;

            while (current != null)
            {
                var tabItem = current as FileTab;
                if (tabItem != null)
                {
                    return tabItem;
                }

                current = VisualTreeHelper.GetParent(current);
            }

            return null;
        }

        private void MetroWindow_DragEnter(object sender, DragEventArgs e)
        {
            MainWindow window = (MainWindow)sender;

            if (window != null)
            {
                //dragNDropPanel.Visibility = Visibility.Visible;

            }
        }

        private void MetroWindow_DragLeave(object sender, DragEventArgs e)
        {
            return;
            //dragNDropPanel.Visibility = Visibility.Hidden;
        }

        private void MetroWindow_Drop(object sender, DragEventArgs e)
        {
            if (e.Data != null && e.Data.GetDataPresent("Defacto.Backoffice.BinaryCracker.FileTab"))
            {
                FileTab source = e.Data.GetData("Defacto.Backoffice.BinaryCracker.FileTab") as FileTab;

                if (source != null)
                {
                    Window sourceWindow = Window.GetWindow(source);

                    if (sourceWindow != this) // Dropped on another window
                    {
                        Console.WriteLine("Dropped on another window");

                        TabControl sourceTabControl = (TabControl)source.Parent;

                        source.IsDragAndDropping = false;

                        sourceTabControl.Items.Remove(source);
                        tabControl.Items.Add(source);
                        tabControl.SelectedItem = source;

                        if (sourceTabControl.Items.Count == 0)
                            sourceWindow.Close();
                    }
                }
            }
        }

        private void MetroWindow_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ((FileTab)tabControl.SelectedItem).GoToLine(int.Parse(lineToGo.Text));
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            foreach (Window window in Application.Current.Windows)
            {
                if (window is MainWindow)
                {
                    ((FileTab)(window as MainWindow).tabControl.SelectedItem).GoToLine(int.Parse(lineToGo.Text));
                }
            }
        }

        private void Search_Click(object sender, RoutedEventArgs e) {
            ((FileTab)tabControl.SelectedItem).Search(searchField.Text);
        }
    }
}
